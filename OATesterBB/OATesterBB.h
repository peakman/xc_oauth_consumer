//
//  OAuthTester.h
//  Migrate
//
//  Created by Steve Clarke on 28/12/2012.
//  Copyright (c) 2012 Steve Clarke. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AppKit/AppKit.h>
#import <OAuthConsumer/OAuthConsumer.h>

#define BITBUCKET @"Bitbucket"
#define OAUTH @"oauth"
#define OAUTH_CONSUMER_SECRET @"OAUTH_CONSUMER_SECRET"
#define OAUTH_CONSUMER_KEY @"OAUTH_CONSUMER_KEY"


@interface OATesterBB : NSObject
-(void) runTests;

@end
