//
//  main.m
//  OAuthTester
//
//  Created by Steve Clarke on 28/12/2012.
//  Copyright (c) 2012 Steve Clarke. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "OATesterBB.h"
int main(int argc, const char * argv[])
{

    @autoreleasepool {
        [[[OATesterBB alloc ] init ]runTests];
        [[NSRunLoop currentRunLoop] runUntilDate: [NSDate dateWithTimeIntervalSinceNow:60.0]];  // Time depends on how quickly you can copy and paste the verifier!
        NSLog(@"Runloop ended");
    }
    return 0;
}

