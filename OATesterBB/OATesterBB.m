//
//  OAuthTester.m
//  Migrate
//
//  Created by Steve Clarke on 28/12/2012.
//  Copyright (c) 2012 Steve Clarke. All rights reserved.
//

#import "OATesterBB.h"
#import <stdlib.h>
#import <time.h>

@implementation OATesterBB
-(void) runTests {
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];

    OAConsumer *consumer = [ [OAConsumer alloc] initWithKey: [defaults valueForKey:OAUTH_CONSUMER_KEY] secret: [defaults valueForKey: OAUTH_CONSUMER_SECRET]];

    
    NSURL *url = [NSURL URLWithString:@"https://bitbucket.org/!api/1.0/oauth/request_token"];
    OAHMAC_SHA1SignatureProvider *provider = [[OAHMAC_SHA1SignatureProvider alloc] init];
    
    
    OAMutableURLRequest *request = [[OAMutableURLRequest alloc] initWithURL: url
                                                                   consumer:consumer
                                                                      token:NULL  // we don't have a Token yet
                                                                      realm:NULL   // our service provider doesn't specify a realm
                                                          signatureProvider: provider
                                                                      nonce: [self generateRandomString]
                                                                  timestamp: [NSString stringWithFormat: @"%.0ld", time(NULL)]
                                    ]; 
    
    [request setHTTPMethod:HTTP_POST];
    
    [request setParameters:@[[[OARequestParameter alloc] initWithName:@"oauth_callback" value:@"oob"]]];
    NSLog(@"Request is %@", request);
    
    [request executeWithCompletionHandler:^(OAServiceTicket *ticket, NSData *data, NSError *error) {
        OAToken *requestToken;
        NSString *responseBody;
        switch (ticket.compCategory) {
            case CC_SUCCESS:
                responseBody = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                requestToken = [[OAToken alloc] initWithHTTPResponseBody:responseBody];
                [self authorizeWithRequestToken: requestToken];
                // give time for user interaction
                [NSTimer scheduledTimerWithTimeInterval: 10.0 target:self selector: @selector(authTimerExpired:) userInfo: requestToken repeats:NO];
                break;
            case CC_URL_ERROR:
                NSLog(@"Requeset token ticket - NSURL error bytes after fail: %@", error);
                break;
            case CC_NO_DATA:
                NSLog(@"Requeset token ticket  - HTTP response code is %ld", [(NSHTTPURLResponse *)ticket.response statusCode]);
        }
    }];
    
}
-(void) getAccessToken: (OAToken *) token {
    NSPasteboard *pasteboard = [NSPasteboard generalPasteboard];
    NSArray *classes = @[[NSString class]];
    NSDictionary *options =nil;
    NSArray *copiedItems = [pasteboard readObjectsForClasses:classes options:options];
    [token setVerifier: copiedItems[0]];
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];

    OAConsumer *consumer = [ [OAConsumer alloc] initWithKey: [defaults valueForKey:OAUTH_CONSUMER_KEY] secret: [defaults valueForKey: OAUTH_CONSUMER_SECRET]];
    
    NSURL *url = [NSURL URLWithString:@"https://bitbucket.org/!api/1.0/oauth/access_token"];
    OAPlaintextSignatureProvider *provider = [[OAPlaintextSignatureProvider alloc] init];
    
    OAMutableURLRequest *request = [[OAMutableURLRequest alloc] initWithURL: url
                                                                   consumer:consumer
                                                                      token: token
                                                                      realm: NULL   // our service provider doesn't specify a realm
                                                          signatureProvider: provider
                                                                      nonce: [self generateRandomString]
                                                                  timestamp: [NSString stringWithFormat: @"%.0ld", time(NULL)]
                                    ]; 
    
    [request setHTTPMethod:HTTP_POST];
    [request executeWithCompletionHandler:^(OAServiceTicket *ticket, NSData *data, NSError *error) {
        NSString *responseBody ;
        OAToken *accessToken;
        switch (ticket.compCategory) {
            case CC_SUCCESS:
                responseBody = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                accessToken = [[OAToken alloc] initWithHTTPResponseBody:responseBody];
                [accessToken storeInUserDefaultsWithServiceProviderName:BITBUCKET prefix: OAUTH];
                NSLog(@"Token stored in user defaults");
                break;
            case CC_URL_ERROR:
                NSLog(@"access token  - NSURL error bytes after fail: %@", error);
                break;
            case CC_NO_DATA:
                NSLog(@"access token   - HTTP response code is %ld", [(NSHTTPURLResponse *)ticket.response statusCode]);
        }
    }];
    [[NSRunLoop currentRunLoop] runUntilDate: [NSDate dateWithTimeIntervalSinceNow:20.0]]; // Should be long enough fro response to arrive
}

-(void) authTimerExpired: (id) timer {
    NSLog(@"Auth time exoired with data %@",[timer userInfo]);
    [self getAccessToken: (OAToken *)[timer userInfo]];
    exit(0);
}


-(void) authorizeWithRequestToken: (OAToken *) token {
    NSString *urls=[NSString stringWithFormat: @"https://bitbucket.org/!api/1.0/oauth/authenticate?oauth_token=%@", [token key]];
    NSURL *url = [NSURL URLWithString: urls];
    [[NSWorkspace sharedWorkspace] openURL:url];  // you need to copy and paste the verifier that should appear in your browser at this point
}
-(NSString *) generateRandomString {
    CFUUIDRef     myUUID;
    CFStringRef   myUUIDString;
    char          strBuffer[100];
    
    myUUID = CFUUIDCreate(kCFAllocatorDefault);
    myUUIDString = CFUUIDCreateString(kCFAllocatorDefault, myUUID);
    
    CFStringGetCString(myUUIDString, strBuffer, 100, kCFStringEncodingASCII);
    
    return [NSString stringWithCString: strBuffer encoding:NSASCIIStringEncoding];
}

@end
