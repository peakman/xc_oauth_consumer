OAuthConsumer Framework
=======================

This is a fork of **Jon Crosby's** OAuthConsumer framework that works with Bitbucket.  It includes some extensions from **Chris Kimpton** to handle the oauth-verifier required by oauth version 1.0a (as used by Bitbucket).  My work was done on OSX 10.8.2 and uses Objective-C ARC. 

I've used the LLVM array and dictionary literals to simplify some things - particulalrly adding parameters to a request.
OADataFetcher has been removed and there is now a method on OAMutableURLRequest  - executeWithCompletionHandler: - that takes a block.

There's a helper (OAHelperBB) that provides a wrapper for some common stuff.

The xcode project also includes some additional code (OATesterBB) that started life as my initial experiments but ended being useful to acquire an authorized access key which it stores using NSUserDefaults.    You need to put OAUTH\_CONSUMER\_KEY and OAUTH\_CONSUMER\_SECRET in ~/Library/Preferences/OATesterBB.plist

Target SC\_OASampleAPI\_BB should execute and demo the new methods.  You need to copy ~/Library/Preferences/OATesterBB.plist as ~/Library/Preferences/SC\_OASample\_BB.plist to use the authenticated access token set by OATesterBB.





