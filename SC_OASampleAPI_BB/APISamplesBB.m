//
//  APISamplesBB.m
//  OAuthConsumer
//
//  Created by Steve Clarke on 30/12/2012.
//
//

#import "APISamplesBB.h"
#import <stdlib.h>
#import <time.h>

@implementation APISamplesBB

-(id) init {
	if (self = [super init]) {
        [self setHelper: [[OAHelperBB alloc] initWithDefaults]];
	}
    return self;
}
-(void) runSamples {
    NSLog(@"Initialized %@", self);
    [self listRepos];
    [self listRepoBranches: @"peakman/xc_migrate"];
    [self createRepository:@"test_repo4" private:@"false"];
}
-(void) listRepos {
    OAMutableURLRequest *request = [[self helper] mutableRequestRelative:@"user/repositories"];
    [request executeWithCompletionHandler:^(OAServiceTicket *ticket, NSData *data, NSError *error) {
        NSArray *jsonObj;
        switch (ticket.compCategory) {
            case CC_SUCCESS:
                jsonObj=[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:NULL];
                NSLog(@"We got data: \n%@", jsonObj);
                break;
            case CC_URL_ERROR:
                NSLog(@"repoList - NSURL error bytes after fail: %@", data);
                break;
            case CC_NO_DATA:
                NSLog(@"listRepos - HTTP response code is %ld", [(NSHTTPURLResponse *)ticket.response statusCode]);
        }
    }];
}
-(void) createRepository: (NSString *) name private: (NSString *) isPrivate {
    NSDictionary *params=@{@"name": name,@"is_private":isPrivate,@"scm":@"git"};
    OAMutableURLRequest *request = [[self  helper ]requestRelative:@"repositories/" method:HTTP_POST params: params];
    [request executeWithCompletionHandler:^(OAServiceTicket *ticket, NSData *data3, NSError *error) {
        switch (ticket.compCategory) {
            case CC_SUCCESS:
                NSLog(@"Created new repo %@\nResponse body is:\n%@",name,[[NSString alloc] initWithData:data3 encoding:NSUTF8StringEncoding] );
                break;
            case CC_URL_ERROR:
                NSLog(@"createRepository - NSURL error bytes after fail: %@", data3);
                break;
            case CC_NO_DATA:
                NSLog(@"createRepository failed - HTTP response code is %ld", [(NSHTTPURLResponse *)ticket.response statusCode]);
        }
    } ];
}

-(void) listRepoBranches: (NSString *) repOwnerSlug  {
    NSString * urlExt=[NSString stringWithFormat:@"repositories/%@/branches", repOwnerSlug] ;
    OAMutableURLRequest *request = [[self helper ]requestRelative: urlExt  method:HTTP_GET params: @{}];
    OARequestCompletionHandler listBranchHandler=^(OAServiceTicket *ticket, NSData *branch_data, NSError *error) {
        switch (ticket.compCategory) {
            case CC_SUCCESS:
                NSLog(@"List repo branches %@\nResponse body is:\n%@",repOwnerSlug,[[NSString alloc] initWithData:branch_data encoding:NSUTF8StringEncoding] );
                break;
            case CC_URL_ERROR:
                NSLog(@"repoList - NSURL error bytes after fail: %@", branch_data);
                break;
            case CC_NO_DATA:
                NSLog(@"listRepos - HTTP response code is %ld", [(NSHTTPURLResponse *)ticket.response statusCode]);
        }
    };
    [request executeWithCompletionHandler:listBranchHandler];
}

@end

