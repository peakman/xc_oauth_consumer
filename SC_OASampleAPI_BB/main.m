//
//  main.m
//  SC_OASampleAPI_BB
//
//  Created by Steve Clarke on 28/12/2012.
//  Copyright (c) 2012 Steve Clarke. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "APISamplesBB.h"
int main(int argc, const char * argv[])
{
    [[[APISamplesBB alloc ] init ]runSamples];
    [[NSRunLoop currentRunLoop] runUntilDate: [NSDate dateWithTimeIntervalSinceNow:10.0]];
    return 0;
}

