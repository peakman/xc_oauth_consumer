//
//  APISamplesBB.h
//  OAuthConsumer
//
//  Created by Steve Clarke on 30/12/2012.
//
//
#import <Foundation/Foundation.h>
#import <OAuthConsumer/OAuthConsumer.h>


#define BITBUCKET @"Bitbucket"
#define OAUTH @"oauth"
#define OAUTH_CONSUMER_SECRET @"OAUTH_CONSUMER_SECRET"
#define OAUTH_CONSUMER_KEY @"OAUTH_CONSUMER_KEY"

@interface APISamplesBB : NSObject

@property (nonatomic,strong) OAHelperBB *helper;


-(void) runSamples;

@end
