//
//  OAMutableURLRequest_OAMutableURLRequestExtensions.h
//  OAuthConsumer
//
//  Created by Steve Clarke on 02/01/2013.
//
//

#import "OAMutableURLRequest_ExecutionExtensions.h"

@implementation  OAMutableURLRequest (OAExecutionExtensions)
-(void) executeWithCompletionHandler: (OARequestCompletionHandler) handler{
    [self prepare];
    OARequestCompletionHandler _handler = [handler copy];
    
    [NSURLConnection sendAsynchronousRequest:self queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *datax, NSError *error) {
        enum CompletionCategory compCat;
        if ( error) {
            compCat=CC_URL_ERROR;
        } else if (([(NSHTTPURLResponse *)response statusCode] > 399)) {
            compCat=CC_NO_DATA;
        } else {
            compCat=CC_SUCCESS;
        }
        OAServiceTicket *serviceTicket = [[OAServiceTicket alloc] initWithRequest: self
                                                                         response: response
                                                               completionCategory: compCat];
        
        _handler(serviceTicket, datax, error);
    }];
}
@end
