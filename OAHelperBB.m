//
//  OAHelperBB.m
//  OAuthConsumer
//
//  Created by Steve Clarke on 31/12/2012.
//
//

#import "OAHelperBB.h"

@implementation OAHelperBB
-(id) initWithDefaults {
	if (self = [super init]) {
        NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
        //[self setSignatureProvider: [[OAPlaintextSignatureProvider alloc  ] init]];
        [self setSignatureProvider: [[OAHMAC_SHA1SignatureProvider alloc  ] init]];
        [self setConsumer: [ [OAConsumer alloc] initWithKey: [defaults valueForKey:OAUTH_CONSUMER_KEY] secret: [defaults valueForKey: OAUTH_CONSUMER_SECRET]]];
        [self setBaseURL: [NSURL URLWithString: [defaults valueForKey: BASE_URL]]];
        [self setAccessToken: [[OAToken alloc ]initWithUserDefaultsUsingServiceProviderName:BITBUCKET prefix:OAUTH]];
	}
    return self;
}

-(OAMutableURLRequest*) mutableRequestRelative: (NSString *) relativeURL {
    return  [[OAMutableURLRequest alloc] initWithURL:[NSURL URLWithString: [relativeURL URLEncodedString ] relativeToURL: [self baseURL]]
                                            consumer:[self consumer]
                                               token:[self accessToken]
                                               realm:nil
                                   signatureProvider:[self signatureProvider]];
}
-(OAMutableURLRequest*) requestRelative: (NSString *) url method: (NSString *) method params: (NSDictionary*) params{
    OAMutableURLRequest *req= [self  mutableRequestRelative: url];
    NSMutableArray *paramArray=[NSMutableArray arrayWithCapacity:10];
    [params enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
        [paramArray addObject:[[OARequestParameter alloc] initWithName: key value: obj]];
    }];
    [req setHTTPMethod: method];
    [req setParameters:paramArray];
    return req;
}



@end
