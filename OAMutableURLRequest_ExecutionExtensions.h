//
//  OAMutableURLRequest_OAMutableURLRequestExtensions.h
//  OAuthConsumer
//
//  Created by Steve Clarke on 02/01/2013.
//
//

#import "OAMutableURLRequest.h"
#import "OAServiceTicket.h"

typedef void (^OARequestCompletionHandler)(OAServiceTicket *ticket, NSData *data, NSError *error);


@interface OAMutableURLRequest (OAExecutionExtensions) 

-(void) executeWithCompletionHandler: (OARequestCompletionHandler) handler ;
@end
