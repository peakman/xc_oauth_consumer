//
//  OAHelperBB.h
//  OAuthConsumer
//
//  Created by Steve Clarke on 31/12/2012.
//
//
#import <Foundation/Foundation.h>
#import <OAuthConsumer/OAuthConsumer.h>

#define BITBUCKET @"Bitbucket"
#define OAUTH @"oauth"
#define OAUTH_CONSUMER_SECRET @"OAUTH_CONSUMER_SECRET"
#define OAUTH_CONSUMER_KEY @"OAUTH_CONSUMER_KEY"
#define BASE_URL @"BASE_URL"
#define HTTP_GET @"GET"
#define HTTP_POST @"POST"
#define HTTP_DELETE @"DELETE"
#define HTTP_PUT @"PUT"

@interface OAHelperBB : NSObject

@property (nonatomic,strong) OAConsumer *consumer;
@property (nonatomic,strong) OAToken *accessToken;
@property (nonatomic,strong) id signatureProvider;
@property (nonatomic,strong) NSURL *baseURL;


-(OAHelperBB*) initWithDefaults;
-(OAMutableURLRequest*) mutableRequestRelative: (NSString *) relativeURL ;
-(OAMutableURLRequest*) requestRelative: (NSString *) url method: (NSString *) method params: (NSDictionary*) params;
@end
